const question = document.querySelector<HTMLBodyElement>('#questions');
const btnA = document.querySelector<HTMLButtonElement>('.A');
const btnB = document.querySelector<HTMLButtonElement>('.B');
const btnC = document.querySelector<HTMLButtonElement>('.C');
const btnD = document.querySelector<HTMLButtonElement>('.D');
const main = document.querySelector<HTMLElement>('#main');
const quiz =  document.querySelector<HTMLElement>('.Quiz');
const resultQuiz = document.querySelector<HTMLElement>('.resultQuiz');
const scoreFinal = document.querySelector<HTMLElement>('.score h3');
const commentaire = document.querySelector<HTMLElement>('.commentaire p');
const trollFace = document.querySelector<HTMLImageElement>('.trollFace img');
const img = document.querySelector<HTMLImageElement>('.illustration img');
let timer = document.querySelector<HTMLElement>('.timer');

// Mes tableau d'image, de question(qcm), de choix de reponse(answers) et de réponses exact(boutonAnswers)
let image: any[] = [
    "https://thumbs.dreamstime.com/b/dessin-de-jambon-dans-le-caract%C3%A8re-avec-l-expression-triste-illustration-d-expressionvector-198796419.jpg",
    "https://i.goopics.net/qgPAd.png", 
    "https://th.bing.com/th/id/R.5af9c23e3cae68862ed0bde551129db3?rik=8V4XKAUUG%2fFFFw&riu=http%3a%2f%2ffr.web.img6.acsta.net%2fr_640_360%2fvideothumbnails%2f195%2f377%2f19537738_20130910101249804.jpg&ehk=iqJgYCsPaPn8cSXBywk3YWXtuMDzNH47aal2Rtjq13I%3d&risl=&pid=ImgRaw&r=0",
    "https://d28mt5n9lkji5m.cloudfront.net/i/W1M85UeZl6.jpg",
    "https://www.blaguehumour.com/medias/images/cendriervstheiere.jpg",
    "https://static.cnews.fr/sites/default/files/festival_cannes_palme.jpg",
    "https://i.pinimg.com/originals/ac/bd/04/acbd0472d2b35ec68898696464c5569d.jpg",
    "https://i.pinimg.com/originals/74/19/96/7419960df5a06a66d76f2730d472864a.jpg",
    "https://th.bing.com/th/id/R.50f6e5691d5ea4e16cdefbded3b96805?rik=nm%2bjbNGCkmkBxw&pid=ImgRaw&r=0",
    "https://www.sospc2424.ch/site/images/web4glossary/image_principale/Fichiers_TS_121367060-5527cd2e649d0.jpg",
];
let qcm: string[] = [
    "Quel est le jambon que tout le monde detestent ?",
    "Comment range t-on les petits pains aux chocolat ?",
    "Qu'a dit Jean Claude Vandam par rapport à l'eau ?",
    "Que se passe t-il quand 2 poissons s'énèrvent ?",
    "Que fait un cendrier devant un ascenseur ?",
    "Comment appelle-t-on un groupement d'aveugle ?",
    "Que dit un ivoirien dans le noir ?",
    "Que dit une lampe en déséspoir ?",
    "Quel est le crustacé le plus léger ?",
    "Que fait un developpeur quand il s'ennuie ?"
];
let answers: string[][] = [
    ["La dinde", "Le poux laid", "le gens bon", "Le sal ami"],
    ["Dans un panier", "Dans l'ordre décroissant", "En vitrine", "Comme on veut"],
    ["L'eau c'est la vie", "L'eau me ressource", "j'ai soif", "Dans 20/30 ans y'en aura plus"], 
    ["Ils se tapent", "Ils se mangent", "Le thon monte", "Le poisson ne crie pas"],
    ["Il veut descendre", "Il squat", "Il fume", "Rien"],
    ["Un groupe d'aveugles", "Le festival de cannes", "Les avugles", "Ca n'existe pas"],
    ["J'y vois rien","Il fait noir","Je suis aveugle","Il fait sombre"],
    ["Une lampe qui parle ?","Je grille","J'ai pas de jus","A LED !"],
    ["Une crevette","Un crabe","Un cloporte","Une palourde"],
    ["il fait un site", "Il dort", "Il se fichier", "Il se plaint"]
];
let boutonAnswers: any[] = [btnD, btnB, btnD, btnC, btnA, btnB, btnA, btnD, btnD, btnC];

//Mes variable de nombre de question(counterQcm), de score et de timer(compteur)
let counterQcm = 0;
let score = 0
let compteur = 15;




questionnaireSuivant();

// 4btn qui correspond aux bouton de mes reponses dans le quiz, les actions qui s'y passe sont des function
btnA?.addEventListener('click', (event) => {
    event.preventDefault();
    if (boutonAnswers[counterQcm] == btnA) {
        //changer couleur en vertevent
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnB?.addEventListener('click', (event) => {
    event.preventDefault();
    if (boutonAnswers[counterQcm] == btnB) {
        //changer couleur en vert
        bonneReponseSC();
    } else {

        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnC?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnC) {
        //changer couleur en vert
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnD?.addEventListener('click', () => {
    console.log("bouton d");
    if (boutonAnswers[counterQcm] == btnD) {
        //changer couleur en vert
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC(); 
    }
    counterQcm++;
    setTimeOut();
})

loadQuiz();


//Function qui permet le temps de transition de chaque question
function setTimeOut() {
    setTimeout(() => {
        displayBlock();
        displayNone();
        questionnaireSuivant();
        if (main) {
            main.style.backgroundColor = 'rgba(255, 255, 255, 0.626)';
        }
    }, 1000);
}
//Function qui affiche la question et réponses actuelles, qui défile grace aux conterQCM
function questionnaireSuivant() {
    if (question && counterQcm < 10) {
        question.innerHTML = qcm[counterQcm];
    }
    if (btnA && btnB && btnC && btnD && counterQcm < 10) {
        btnA.innerHTML = answers[counterQcm][0];
        btnB.innerHTML = answers[counterQcm][1];
        btnC.innerHTML = answers[counterQcm][2];
        btnD.innerHTML = answers[counterQcm][3];
        compteur= 15;
        console.log(compteur , "compteur");
    };
    if(img) {
        img.src = image[counterQcm];
        console.log(image[counterQcm]);
    }
    if(counterQcm){
        compteur = 15
        if(timer)
        timer.textContent = String(compteur)
    }
    console.log("competur");
}  
// Ces deux functions permettent d'afficher le contenu du score et de faire disparaitre le contenue du qcm  
function displayNone() {

    if(counterQcm > 9){
        
        if(quiz) {
            quiz.style.display = 'none'
        } 
    } else {
        if(quiz) {
            quiz.style.display = 'block'
        }
    }
}
function displayBlock() {

    if(counterQcm < 10) {
        if(resultQuiz) {
            resultQuiz.style.display = ''
        } 
    } else {
        if(resultQuiz) {
            resultQuiz.style.display = 'block'
        }
    }
}

// Ces deux functions permettent le changement de couleur du bg selon la bonne ou mauvaise reponse et 
// d'afficher le score
function bonneReponseSC() {
    if (main) {
        main.style.backgroundColor = 'rgba(0, 128, 0, 0.628)';
        score++;
        if (scoreFinal) {
            scoreFinal.innerHTML = 'Score : ' + score + '/10';
            commentaireFinal();
        }
    }
   
    
}
function mauvaiseReponseSC() {
    if (main) {
        main.style.backgroundColor = 'rgba(255, 0, 0, 0.523)';
        score -= 2;
        if (scoreFinal) {
            scoreFinal.innerHTML = 'Score : ' + score + '/10';
            commentaireFinal();
        }
    }
}
// Cette function permet d'afficher le commentaire et l'image avec le score à la fin du qcm
function commentaireFinal() {
    if (score <= 5 && score >= 0) {
        if (commentaire) {
            commentaire.innerHTML = 'Travail encore t/es blagues';
        }
        if(trollFace)
        {
            trollFace.src = "https://www.nicepng.com/png/full/315-3150192_crying-troll-face-png.png";
        }
    } else if (score > 5) {
        if (commentaire) {
            commentaire.innerHTML = 'Crois pas t drole';
        }
        if(trollFace)
        {
            trollFace.src = "https://www.pintarmewarnai.com/png/thumb/efmo0a5UseqS7q7-Trollface-PNG-HD.png";
        } 
    } else if (score < 0) {
        if (commentaire) {
            commentaire.innerHTML = 'Négatif ? Tu veux en parler ?';
        }
        if(trollFace)
        {
            trollFace.src = "https://toppng.com/uploads/preview/trollface-115507175970pk83porcj.png";
        }
    } 
}


 // Le timer du quiz permet de faire le décompte et de pouvoir changer de question à la fin du décompte  
function loadQuiz() {
    setInterval(function () {

        if (compteur == -1) {
            displayBlock();
            displayNone();
            counterQcm++;
            questionnaireSuivant();
            
            compteur = 15;
            //id est la question de tableau, quand id = 0, on prend toutes les questions.     
        }
        if (timer) {
            timer.textContent = String(compteur--);
            console.log("competur", compteur);
            
        }
    }, 1000);
}

