const question = document.querySelector<HTMLBodyElement>('#questions');
const btnA = document.querySelector<HTMLButtonElement>('.A');
const btnB = document.querySelector<HTMLButtonElement>('.B');
const btnC = document.querySelector<HTMLButtonElement>('.C');
const btnD = document.querySelector<HTMLButtonElement>('.D');
const main = document.querySelector<HTMLElement>('#main');
const quiz =  document.querySelector<HTMLElement>('.Quiz');
const resultQuiz = document.querySelector<HTMLElement>('.resultQuiz');
const scoreFinal = document.querySelector<HTMLElement>('.score h3');
const commentaire = document.querySelector<HTMLElement>('.commentaire p');
const trollFace = document.querySelector<HTMLImageElement>('.trollFace img');
const img = document.querySelector<HTMLImageElement>('.illustration img');
let timer = document.querySelector<HTMLElement>('.timer');

// Mes tableau d'image, de question(qcm), de choix de reponse(answers) et de réponses exact(boutonAnswers)
let image: any[] = [
    "https://i.pinimg.com/originals/58/f1/4b/58f14b7ac7aaf06cf2b9c5adba03ddc2.jpg",
    "https://pbs.twimg.com/media/FREna9IXwAMw3QV.jpg", 
    "https://www.japanfm.fr/wp-content/uploads/2022/12/luffy-et-roger-scaled.webp",
    "https://i.pinimg.com/originals/11/a1/54/11a154aa66c7c404257dfb0112e9643f.jpg",
    "https://preview.redd.it/hwev0baxbv571.png?width=640&crop=smart&auto=webp&s=8a480ccfb4314054321d0a8c9c93299302ebbe2b",
    "https://images2.alphacoders.com/516/516664.jpg",
    "https://www.japanfm.fr/wp-content/uploads/2022/06/one-piece-odyssey.jpg",
    "https://c4.wallpaperflare.com/wallpaper/877/494/335/one-piece-monkey-d-luffy-monkey-d-dragon-monkey-d-garp-wallpaper-preview.jpg",
    "https://areajugones.sport.es/wp-content/uploads/2020/09/one-piece-ace-1080x609.jpg",
    "https://www.melty.fr/wp-content/uploads/meltyfr/2022/06/capture-decran-2022-06-08-a-14.38.31-736x398.png",
];
let qcm: string[] = [
    "En quelle année est sortie One piece ?",
    "Qui est ce personnage ?",
    "Quel est son but ?",
    "Qui est son second ?",
    "Qui est l'auteur de cette oeuvre ?",
    "Qui sont ces frères ?",
    "Que signifie le One piece ?",
    "Quel est le membre de sa famille qu'il a perdu ?",
    "Comment s'appel le frère qu'il a perdu ?",
    "De qui tien t-il son chapeau de paille ?"
];
let answers: string[][] = [
    ["En 1999", "2002", "2005", "1995"],
    ["Sasuke", "Zoro", "Naruto", "luffy"],
    ["Devenir le plus fort", "Devenir le roi", "Rendre son chapeau", "Détruire le monde"], 
    ["Tête de cactus", "sourcil en spiral", "Le gros nez", "Le tanuki"],
    ["Eichiiro Oda", "Masashi kishimoto", "Osamu Tezuka", "Kentaro Miura"],
    ["Zoro et Sanji", "Garp et Dragon", "Ace et Sabo", "Il n'a pas de frère"],
    ["Une piece","De l'or","Le plus grand mystère","Son chapeau"],
    ["Son père ?","Son frère","Sa mère","Son grand père"],
    ["Sabo","Sanji","Ussop","Ace"],
    ["Du roi des pirate", "De Shanks", "De Rayleigh", "De Jimbe"]
];
let boutonAnswers: any[] = [btnA, btnD, btnB, btnA, btnA, btnC, btnC, btnB, btnD, btnB];

//Mes variable de nombre de question(counterQcm), de score et de timer(compteur)
let counterQcm = 0;
let score = 0
let compteur = 15;


questionnaireSuivant();

// 4btn qui correspond aux bouton de mes reponses dans le quiz, les actions qui s'y passe sont des function
btnA?.addEventListener('click', (event) => {
    event.preventDefault();
    if (boutonAnswers[counterQcm] == btnA) {
        //changer couleur en vertevent
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnB?.addEventListener('click', (event) => {
    event.preventDefault();
    if (boutonAnswers[counterQcm] == btnB) {
        //changer couleur en vert
        bonneReponseSC();
    } else {

        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnC?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnC) {
        //changer couleur en vert
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnD?.addEventListener('click', () => {
    console.log("bouton d");
    if (boutonAnswers[counterQcm] == btnD) {
        //changer couleur en vert
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC(); 
    }
    counterQcm++;
    setTimeOut();
})

loadQuiz();

//Function qui permet le temps de transition de chaque question
function setTimeOut() {
    setTimeout(() => {
        displayBlock();
        displayNone();
        questionnaireSuivant();
        if (main) {
            main.style.backgroundColor = 'rgba(255, 255, 255, 0.626)';
        }
    }, 1000);
}
//Function qui affiche la question et réponses actuelles, qui défile grace aux conterQCM
function questionnaireSuivant() {
    if (question && counterQcm < 10) {
        question.innerHTML = qcm[counterQcm];
    }
    if (btnA && btnB && btnC && btnD && counterQcm < 10) {
        btnA.innerHTML = answers[counterQcm][0];
        btnB.innerHTML = answers[counterQcm][1];
        btnC.innerHTML = answers[counterQcm][2];
        btnD.innerHTML = answers[counterQcm][3];
        compteur= 15;
        console.log(compteur , "compteur");
    };
    if(img) {
        img.src = image[counterQcm];
        console.log(image[counterQcm]);
    }
    if(counterQcm){
        compteur = 15
        if(timer)
        timer.textContent = String(compteur)
    }
    console.log("competur");
}  
// Ces deux functions permettent d'afficher le contenu du score et de faire disparaitre le contenue du qcm
function displayNone() {

    if(counterQcm > 9){
        
        if(quiz) {
            quiz.style.display = 'none'
        } 
    } else {
        if(quiz) {
            quiz.style.display = 'block'
        }
    }
}
function displayBlock() {

    if(counterQcm < 10) {
        if(resultQuiz) {
            resultQuiz.style.display = ''
        } 
    } else {
        if(resultQuiz) {
            resultQuiz.style.display = 'block'
        }
    }
}

// Ces deux functions permettent le changement de couleur du bg selon la bonne ou mauvaise reponse et 
// d'afficher le score
function bonneReponseSC() {
    if (main) {
        main.style.backgroundColor = 'rgba(0, 128, 0, 0.628)';
        score++;
        if (scoreFinal) {
            scoreFinal.innerHTML = 'Score : ' + score + '/10';
            commentaireFinal();
        }
    }
   
    
}
function mauvaiseReponseSC() {
    if (main) {
        main.style.backgroundColor = 'rgba(255, 0, 0, 0.523)';
        score -= 2;
        if (scoreFinal) {
            scoreFinal.innerHTML = 'Score : ' + score + '/10';
            commentaireFinal();
        }
    }
}
// Cette function permet d'afficher le commentaire et l'image avec le score à la fin du qcm
function commentaireFinal() {
    if (score <= 5 && score >= 0) {
        if (commentaire) {
            commentaire.innerHTML = 'Un score vreuuumant...';
        }
        if(trollFace)
        {
            trollFace.src = "https://ih1.redbubble.net/image.3878337406.2496/st,small,507x507-pad,600x600,f8f8f8.jpg";
        }
    } else if (score > 5) {
        if (commentaire) {
            commentaire.innerHTML = 'Mon coeur bat !';
        }
        if(trollFace)
        {
            trollFace.src = "https://w7.pngwing.com/pngs/278/791/png-transparent-vinsmoke-sanji-monkey-d-luffy-roronoa-zoro-anime-one-piece-vinsmoke-sanji-one-piece-manga-chibi-computer-wallpaper-thumbnail.png";
        } 
    } else if (score < 0) {
        if (commentaire) {
            commentaire.innerHTML = 'Négatif ? Tu mérite la même chose';
        }
        if(trollFace)
        {
            trollFace.src = "https://e7.pngegg.com/pngimages/600/698/png-clipart-vinsmoke-sanji-monkey-d-luffy-usopp-facepalm-one-piece-one-piece-black-hair-hand.png";
        }
    } 
}


 // Le timer du quiz permet de faire le décompte et de pouvoir changer de question à la fin du décompte  
function loadQuiz() {
    setInterval(function () {

        if (compteur == -1) {
            displayBlock();
            displayNone();
            counterQcm++;
            questionnaireSuivant();
            
            compteur = 15;
            //id est la question de tableau, quand id = 0, on prend toutes les questions.     
        }
        if (timer) {
            timer.textContent = String(compteur--);
            console.log("competur", compteur);
            
        }
    }, 1000);
}



