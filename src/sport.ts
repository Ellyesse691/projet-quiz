const question = document.querySelector<HTMLBodyElement>('#questions');
const btnA = document.querySelector<HTMLButtonElement>('.A');
const btnB = document.querySelector<HTMLButtonElement>('.B');
const btnC = document.querySelector<HTMLButtonElement>('.C');
const btnD = document.querySelector<HTMLButtonElement>('.D');
const main = document.querySelector<HTMLElement>('#main');
const quiz =  document.querySelector<HTMLElement>('.Quiz');
const resultQuiz = document.querySelector<HTMLElement>('.resultQuiz');
const scoreFinal = document.querySelector<HTMLElement>('.score h3');
const commentaire = document.querySelector<HTMLElement>('.commentaire p');
const trollFace = document.querySelector<HTMLImageElement>('.trollFace img');
const img = document.querySelector<HTMLImageElement>('.illustration img');
let timer = document.querySelector<HTMLElement>('.timer');

// Mes tableau d'image, de question(qcm), de choix de reponse(answers) et de réponses exact(boutonAnswers)
let image: any[] = [
    "https://i1.wp.com/www.bike-urious.com/wp-content/uploads/Spain-and-Portugal-Flags.jpg?fit=881%2C480&ssl=1",
    "https://img.staticdj.com/800495185a268cb59d2e92d3e97d8b8a_1080x_nw.jpeg", 
    "https://i.pinimg.com/originals/00/c7/6c/00c76c70f7bb85f20b2d0b242c644fed.jpg",
    "https://th.bing.com/th/id/R.f87f76e32ed3dba9fa77584141d0ab14?rik=trBq9yTB6FCPNw&pid=ImgRaw&r=0",
    "https://yesofcorsa.com/wp-content/uploads/2018/02/Sports-Nutrition-Wallpaper-For-PC.jpg",
    "https://1.bp.blogspot.com/-dJVoR0YYyz0/XPe-HW3DjXI/AAAAAAAAAXM/v-eQ3pnweqUQYG7Z9KUb-wIpHDqCFIpUgCLcBGAs/s1600/How%2Bto%2BStart%2BYour%2BOwn%2BSports%2BBetting%2BBusiness%2Band%2BMake%2Ba%2BKilling.jpg",
    "https://th.bing.com/th/id/OIP.mAzH3bLIXqXie60whelTlgHaEK?pid=ImgDet&rs=1",
    "https://xxhuyuzero.jp/wp-content/uploads/2017/02/4ed76586-s.jpg",
    "https://images6.alphacoders.com/312/thumbbig-312376.jpg",
    "https://th.bing.com/th/id/R.5ec2f4d2d056cce497ec2939e6f4d0c1?rik=tDIxexsclHEjtg&riu=http%3a%2f%2fwallpapercave.com%2fwp%2f6rteOa9.jpg&ehk=s8ojup%2bGgUlgtrEQVOphDPh4npbgzxLsFuCGMIFUjrY%3d&risl=&pid=ImgRaw&r=0",
];
let qcm: string[] = [
    "Quel équipe qui a battue l'Espagne et le Portugal ?",
    "Qui a été élu meilleur buteur de la CDM 2022 ?",
    "Quand a eu lieu la première Coupe du monde ?",
    "Quel est le sport national au Japon ?",
    "Combien de temps entre le sport et un repas ?",
    "Quel sport fait perdre le plus de poids ?",
    "Quel est le nageur le plus rapide ?",
    "Qui est ce champion de patinage 2014 et 2017 ?",
    "Quel est le style de nage le plus rapide ?",
    "quel est le meilleur joueur de basket ?"
];
let answers: string[][] = [
    ["Le Maroc", "La France", "La Croatie", "Le Canada"],
    ["Messi", "Ronaldo", "Mbappé", "Benzema"],
    ["1930", "1920", "1950", "1970"],
    ["Le Karate", "Le Sumo", "Le Judo", "Le Ninjutsu"],
    ["2 heures", "1 heure 30", "4 heures", "3 heures"],
    ["Course à pied", "Course de haie", "Basket-ball", "Natation"],
    ["Bobby Hackett","Francis Haas","Michael Pheps","Paul Hait"],
    ["Chiharu Shiota","Yuzuru Hanyu","Hiroshi Senju","Yokoyama Taikan"],
    ["crawl","Le papillon","Crawl dos","La brasse"],
    ["Tony Parker", "Yao Ming", "Kareem Abdul-Jabbar", "Michael Jordan"]
];
let boutonAnswers: any[] = [btnA, btnC, btnA, btnB, btnD, btnD, btnC, btnB, btnA, btnD];

//Mes variable de nombre de question(counterQcm), de score et de timer(compteur)
let counterQcm = 0;
let score = 0
let compteur = 15;



questionnaireSuivant();

// 4btn qui correspond aux bouton de mes reponses dans le quiz, les actions qui s'y passe sont des function
btnA?.addEventListener('click', (event) => {
    event.preventDefault();
    if (boutonAnswers[counterQcm] == btnA) {
        //changer couleur en vertevent
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnB?.addEventListener('click', (event) => {
    event.preventDefault();
    if (boutonAnswers[counterQcm] == btnB) {
        //changer couleur en vert
        bonneReponseSC();
    } else {

        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnC?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnC) {
        //changer couleur en vert
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC();
    }
    counterQcm++;
    setTimeOut();
})

btnD?.addEventListener('click', () => {
    console.log("bouton d");
    if (boutonAnswers[counterQcm] == btnD) {
        //changer couleur en vert
        bonneReponseSC();
    } else {
        //changer couleur en rouge
        mauvaiseReponseSC(); 
    }
    counterQcm++;
    setTimeOut();
})

loadQuiz();


//Function qui permet le temps de transition de chaque question
function setTimeOut() {
    setTimeout(() => {
        displayBlock();
        displayNone();
        questionnaireSuivant();
        if (main) {
            main.style.backgroundColor = 'rgba(255, 255, 255, 0.626)';
        }
    }, 1000);
}
//Function qui affiche la question et réponses actuelles, qui défile grace aux conterQCM
function questionnaireSuivant() {
    if (question && counterQcm < 10) {
        question.innerHTML = qcm[counterQcm];
    }
    if (btnA && btnB && btnC && btnD && counterQcm < 10) {
        btnA.innerHTML = answers[counterQcm][0];
        btnB.innerHTML = answers[counterQcm][1];
        btnC.innerHTML = answers[counterQcm][2];
        btnD.innerHTML = answers[counterQcm][3];
        compteur= 15;
        console.log(compteur , "compteur");
    };
    if(img) {
        img.src = image[counterQcm];
        console.log(image[counterQcm]);
    }
    if(counterQcm){
        compteur = 15
        if(timer)
        timer.textContent = String(compteur)
    }
    console.log("competur");
}    
// Ces deux functions permettent d'afficher le contenu du score et de faire disparaitre le contenue du qcm
function displayNone() {

    if(counterQcm > 9){
        
        if(quiz) {
            quiz.style.display = 'none'
        } 
    } else {
        if(quiz) {
            quiz.style.display = 'block'
        }
    }
}
function displayBlock() {

    if(counterQcm < 10) {
        if(resultQuiz) {
            resultQuiz.style.display = ''
        } 
    } else {
        if(resultQuiz) {
            resultQuiz.style.display = 'block'
        }
    }
}

// Ces deux functions permettent le changement de couleur du bg selon la bonne ou mauvaise reponse et 
// d'afficher le score
function bonneReponseSC() {
    if (main) {
        main.style.backgroundColor = 'rgba(0, 128, 0, 0.628)';
        score++;
        if (scoreFinal) {
            scoreFinal.innerHTML = 'Score : ' + score + '/10';
            commentaireFinal();
        }
    }
   
    
}
function mauvaiseReponseSC() {
    if (main) {
        main.style.backgroundColor = 'rgba(255, 0, 0, 0.523)';
        score -= 2;
        if (scoreFinal) {
            scoreFinal.innerHTML = 'Score : ' + score + '/10';
            commentaireFinal();
        }
    }
}
// Cette function permet d'afficher le commentaire et l'image avec le score à la fin du qcm
function commentaireFinal() {
    if (score <= 5 && score >= 0) {
        if (commentaire) {
            commentaire.innerHTML = 'Tu serai pas sportif du dimanche ?';
        }
        if(trollFace)
        {
            trollFace.src = "https://cdn.imgbin.com/18/3/1/imgbin-trollface-rage-comic-internet-troll-internet-meme-meme-7bp6apueKnkAyqzET0aU2kiXq.jpg";
        }
    } else if (score > 5) {
        if (commentaire) {
            commentaire.innerHTML = 'On a un haut niveau ici ?';
        }
        if(trollFace)
        {
            trollFace.src = "https://www.clipartkey.com/mpngs/b/182-1827781_troll-face-clipart.png";
        } 
    } else if (score < 0) {
        if (commentaire) {
            commentaire.innerHTML = 'Tu sais ce qu\ est le sport ?';
        }
        if(trollFace)
        {
            trollFace.src = "https://th.bing.com/th/id/R.a8d5442bb4c070ee3a30550d743411cc?rik=gurxvcABDJP75A&riu=http%3a%2f%2fgetdrawings.com%2fvectors%2ftroll-face-vector-13.jpg&ehk=v3%2fvc53cQ0p%2fpe5PK9GGp0toDWB7fTkIv4meCmjWyC0%3d&risl=&pid=ImgRaw&r=0";
        }
    } 
}

// Le timer du quiz permet de faire le décompte et de pouvoir changer de question à la fin du décompte 
function loadQuiz() {
    setInterval(function () {

        if (compteur == -1) {
            displayBlock();
            displayNone();
            counterQcm++;
            questionnaireSuivant();
            
            compteur = 15;
            //id est la question de tableau, quand id = 0, on prend toutes les questions.     
        }
        if (timer) {
            timer.textContent = String(compteur--);
            console.log("competur", compteur);
            
        }
    }, 1000);
}


