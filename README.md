# DOM
Projet dans lequel on voit le DOM avec typescript

# Projet Quizz

Créer une application de type quizz/QCM/questionnaire sur le thème de votre choix (en gardant toujours en tête que 
cette application pourrait être consultée par un·e employeur·se).

## Fonctionnalités obligatoire
* Affichage des questions et choix de la réponse
* Enchaînement de plusieurs questions (sans changement de page)
* Décompte des points et affichage du score (en temps réel, et/ou à la fin)
* Responsive

Ne pas hésiter à rajouter d'autres fonctionnalités une fois celles ci implémentées (timer, plusieurs réponses possibles, réponse en champ de texte, illustrations pour certaines questions, etc.)

### Compétences à mobilisées
Dans l'idéal, vous devrez utiliser des variables, conditions, tableaux, boucles et fonctions. Manipulation du DOM (querySelector / Events).
Essayer de maintenir votre code DRY.

## Réalisation

Commencer par trouver le thème de votre quizz et une liste de questions pour celui ci.

Ensuite réaliser une ou plusieurs maquettes fonctionnelles de ce à quoi ressemblera l'application.

Vous devrez rendre un projet gitlab avec un README présentant le projet et les maquettes (et à terme pourquoi pas 
une explication de votre méthodologie pour le code)

Commentez votre code avec au moins la JS doc de vos fonctions.

-----------------------------------------------------------------------------------------------------
Voici mon projet Quiz. Il contient Trois thème de quiz dans chaque page différentes.
Il y'a 10 questions pour chacun des thèmes avec 4 choix de réponses, il y a une image qui correspond au sujet de la question. Il y a un Timer qui permet le décompte de chaque question laissant un temps de reflexion de 15sec après ça, on passe à la question suivante. On peut aussi passer aux questions suivantes en cliquant sur un des 4 boutons reponses. 
A la fin votre score, avec une illustration et un commentaire apparait.


lien gitlab : https://gitlab.com/Ellyesse691 